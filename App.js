import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MainScreen from './screens/main';
import SearchScreen from './screens/search';
import { NavigationContainer } from '@react-navigation/native';
import InfoScreen from './screens/info';
import GraphScreen from './screens/graphscreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';



const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
    <Tab.Navigator>
      <Tab.Screen name="Main" component={MainScreen} options={{tabBarLabel:"Main",tabBarIcon:({color,size})=>(<Icon name="home" color={"white"} size={25}/>)}}/>
      <Tab.Screen name="Search" component={SearchScreen} options={{tabBarLabel:"Search",tabBarIcon:({color,size})=>(<Icon name="file-find-outline" color={"white"} size={25}/>)}}/>
      <Tab.Screen name="Graph" component={GraphScreen} options={{tabBarLabel:"Graph",tabBarIcon:({color,size})=>(<Icon name="chart-bar" color={"white"} size={25}/>)}}/>
      <Tab.Screen name="Information" component={InfoScreen} options={{tabBarLabel:"Information",tabBarIcon:({color,size})=>(<Icon name="information-outline" color={"white"} size={25}/>)}}/>
    </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000080',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});
