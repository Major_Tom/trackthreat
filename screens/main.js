import React from 'react';
import { Text, View,StyleSheet } from 'react-native'
import { set } from 'react-native-reanimated';




export default class MainScreen extends React.Component{
    constructor(props){
        super(props)
        this.state={Global:{TotalConfirmed:0,TotalRecovered:0,TotalDeaths:0}}
    }
    componentDidMount(){
        const url = "https://api.covid19api.com/summary"
        console.log("componentDidMount")
        fetch(url).then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson)
            this.setState(responseJson)
            })
    }
    render(){
        console.log(this.state.Global)
        console.log("render")
        return(
            <View style ={{flex: 1}}>
            <Text style={styles.text1}>Total confirmed cases</Text>
            <Text style={styles.text2}>{this.state.Global.TotalConfirmed}</Text>
            <Text style={styles.text1}>Total recovered cases</Text>
            <Text style={styles.text2}>{this.state.Global.TotalRecovered}</Text>
            <Text style={styles.text1}>Total death</Text>
            <Text style={styles.text2}>{this.state.Global.TotalDeaths}</Text>
            <Text style={styles.text3}>Date</Text>
            <Text style={styles.text4}>{this.state.Date}</Text>
            </View>
        )
    }

    
}
const styles = StyleSheet.create({
    
    text1 : {
      fontSize : 25,
      color : 'Grey',
      margin : 1,
      textAlign : "center",
      marginTop : '20%'
    },
    text2 : {
        fontSize : 25,
        color : 'Grey',
        margin : 5,
        textAlign : "center"
    },
    text3 : {
        fontSize : 20,
        color : 'Grey',
        margin : 5,
        textAlign : "center",
        marginTop : '15%'
      },
      text4 : {
        fontSize : 20,
        color : 'Grey',
        margin : 5,
        textAlign : "center"
      }
  });
